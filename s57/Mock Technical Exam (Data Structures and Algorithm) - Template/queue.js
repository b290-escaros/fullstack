let collection = [];

function print() {
  return collection;
}

function enqueue(element) {
  collection.push(element);
  return collection;
}

function dequeue() {
  if (isEmpty()) {
    return "Underflow";
  }
  collection.shift();
  return collection;
}

function front() {
  if (isEmpty()) {
    return "No elements in the queue";
  }
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length === 0;
}

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};

