// alert("Hello")
// querySelector() is a method that can be used to select a specific object/element from our document.

console.log(document.querySelector("#txt-first-name"))
console.log(document);

/*
	Alternative ways to access HTML elements. This is what we can use aside from the querySelector().

	document.getElementById("txt-first-name");
	document.getElementsByClassName("txt-first-name");
	document.getElementsByTagName("input");
*/

console.log(document.getElementById("txt-first-name"))

/*=============== EVENT & EVENT Listener================*/

const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")

console.log(txtFirstName.value)
console.log(spanFullName.value)

/*

	Event
		ex: clock, hover, keypress and many other events

	Event Listeners
		Allows us to let our user/s interact with our page. With each click or hover there is an event which triggers a function/task.

	Syntax:
		selectedElement.addEventListener("event", function);

*/

// Add event listeners to the input elements
txtFirstName.addEventListener('keyup',(event)=>{
	// "innerHTML" property retrieves the HTML content/children within the element
	// "value" property retrieves the value from the HTML element
	spanFullName.innerHTML = txtFirstName.value
})

// Alternative way to write a code for event handling
txtLastName.addEventListener("keyup", printLastName);

function printLastName(event){
	spanFullName.innerHTML = txtLastName.value;
}

txtFirstName.addEventListener("keyup", event =>{
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
})


/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened.
	the "event.target.value" gets the value of the input object (this is similar to txtFirstName.value).
*/

// MINI ACTIVITY
// CREATE AN EVENT LISTENER 

/*IF the first label is clicked an alert message " You cliked First Name labe" will prompt*/


txtFirstName.addEventListener('click', () => {
  alert('You clicked First Name label');
});